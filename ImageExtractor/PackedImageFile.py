import struct

from RawImage import RawImage


class PackedImageFile:
    def __init__(self, file_stream):
        self.__file_stream = file_stream

    def extract_images(self):
        if self.__is_header_valid() is not True:
            raise Exception('Header mismatch: File corrupted')

        num_images = self.__read_unsigned_int()
        if 6 != num_images:
            raise Exception('Length mismatch: File corrupted')

        extracted_images = []
        for i in range(0, num_images):
            extracted_images.append(self.__extract_image())

        return extracted_images

    def __extract_image(self):
        image_name = self.__read_zero_terminated_string()
        width = self.__read_unsigned_int()
        height = self.__read_unsigned_int()
        bands = self.__file_stream.read(4)

        if bands not in RawImage.bands_to_type_mapping.keys():
            raise Exception('Unsupported image type detected')

        stride_size = RawImage.bands_to_stride_size_mapping[bands]

        raw_image_size_in_bytes = width * height * stride_size

        data = self.__file_stream.read(raw_image_size_in_bytes)

        return RawImage(width, height, bands, image_name, data)

    def __read_zero_terminated_string(self):
        buffer = b''
        zero_terminator = struct.pack('B', 0)

        while True:
            cur_byte = self.__file_stream.read(1)
            if cur_byte == zero_terminator:
                return buffer.decode()

            buffer = buffer + cur_byte

    def __read_unsigned_int(self):
        return struct.unpack('I', self.__file_stream.read(4))[0]

    def __is_header_valid(self):
        expected_header = struct.pack('ccBBccc', b'S', b'P', 0, 1, b'i', b'm', b'g')

        header = self.__file_stream.read(7)

        return header == expected_header
