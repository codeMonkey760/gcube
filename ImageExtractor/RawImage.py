import struct
from PIL import Image


class RawImage:
    RGB = struct.pack('bbbb', 1, 1, 1, 0)
    RGBA = struct.pack('bbbb', 1, 1, 1, 1)

    bands_to_stride_size_mapping = {
        RGB: 3,
        RGBA: 4
    }

    bands_to_type_mapping = {
        RGB: 'RGB',
        RGBA: 'RGBA'
    }

    def __init__(self, width, height, bands, file_name, data):
        self.__width = width
        self.__height = height
        self.__bands = bands
        self.__file_name = file_name
        self.__data = data

    def save_image_to_file(self):
        mode = RawImage.bands_to_type_mapping[self.__bands]
        size = (self.__width, self.__height)

        image = Image.frombytes(mode, size, self.__data, "raw", mode)

        image.save(self.__file_name)
