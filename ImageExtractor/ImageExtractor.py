from PackedImageFile import PackedImageFile


class ImageExtractor:
    def run(self, packed_images_file_name):
        packed_file = PackedImageFile(open(packed_images_file_name, 'rb'))

        extracted_images = packed_file.extract_images()

        for image in extracted_images:
            image.save_image_to_file()
