/*
Copyright 2016 Sean Paget
Email: codeMonkey760@hotmail.com
    
This file is part of gCube.

gCube is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gCube is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gCube.  If not, see <http://www.gnu.org/licenses/>.
Please read COPYING.txt for details
*/

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "Camera.h"
#include "Util.h"
#include "Camera.h"
#include "Cubelet.h"
#include "Cube.h"
#include "GUIButton.h"
#include "GUIRenderer.h"
#include "GUIShader.h"

GUIRenderer guiRenderer;

void InitGUIRenderer (void) {
    memset(&guiRenderer, 0, sizeof(GUIRenderer));
    
    _BuildVBO();
}

void DrawButtons (GUIButton *buttons, int numButtons) {
    int i;
    GUIButton *cb = NULL;
    if (buttons == NULL || numButtons < 1 || guiShader.shader == -1) return;
    
    glUseProgram(guiShader.shader);

    glBindVertexArray(guiRenderer.buttonVAO);

    for (i = 0; i < numButtons; ++i) {
        cb = &(buttons[i]);
        if (
            cb == NULL ||
            cb->visible == false ||
            cb->texId == -1
        ) continue;
        
        glUniformMatrix4fv(guiShader.gTMtx,1,GL_TRUE,cb->tMtx);
        glUniformMatrix3fv(guiShader.gTexMtx, 1, GL_TRUE, cb->texMtx);
        glUniform1f(guiShader.gAlphaCutoff, 0.70f);
        glUniform1i(guiShader.gTexture, 0);
        glUniform3fv(guiShader.gAmbient,1,cb->ambientColor);
        glUniform1f(guiShader.gMixValue, cb->mixValue);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, cb->texId);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    glBindVertexArray(0);
    glUseProgram(0);
}

void DestroyGUIRenderer (void) {
    glDeleteVertexArrays(1, &guiRenderer.buttonVAO);
    guiRenderer.buttonVAO = -1;

    glDeleteBuffers(1, &guiRenderer.buttonVBO);
    guiRenderer.buttonVBO = -1;
}

void _BuildVBO (void) {
    float vb[] = {
        -1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f,-1.0f, 0.0f, 0.0f, 1.0f,
         1.0f,-1.0f, 0.0f, 1.0f, 1.0f,
        
         1.0f,-1.0f, 0.0f, 1.0f, 1.0f,
         1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f, 0.0f, 0.0f
    };

    glGenVertexArrays(1, &guiRenderer.buttonVAO);

    glBindVertexArray(guiRenderer.buttonVAO);
    
    glGenBuffers(1,&guiRenderer.buttonVBO);
    
    glBindBuffer(GL_ARRAY_BUFFER,guiRenderer.buttonVBO);
    glBufferData(GL_ARRAY_BUFFER, 6 * 5 * 4, vb, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE, 20, 0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE, 20, (const void *) 12);

    glBindVertexArray(0);
}

